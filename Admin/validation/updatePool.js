const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateUpdatePoolInput(data) {
    let errors = {};
    data.tokenSymbol = !isEmpty(data.tokenSymbol) ? data.tokenSymbol : "";
    data.tokenAddresses = !isEmpty(data.tokenAddresses) ? data.tokenAddresses : "";
    data.quoteTokenSymbol = !isEmpty(data.quoteTokenSymbol) ? data.quoteTokenSymbol : "";
    data.quoteTokenAdresses = !isEmpty(data.quoteTokenAdresses) ? data.quoteTokenAdresses : "";
    //data.file = !isEmpty(data.file) ? data.file : "";
    if (Validator.isEmpty(data.tokenSymbol)) {
        errors.tokenSymbol = "Token Symbol field is required";
    }
    if (Validator.isEmpty(data.tokenAddresses,{ max: 42 })) {
        errors.tokenAddresses = "Token Address field is required";
    } 
    if (data.tokenAddresses.length != 42) {
        errors.tokenAddresses = "Address field Must have 42 letters";
   }
return {
        errors,
        isValid: isEmpty(errors)
    };
};