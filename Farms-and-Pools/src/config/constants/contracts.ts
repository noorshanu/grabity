export default {
  cake: {
    56: '0xD800D37C533B157DC1Cb5B6907931DAAF93B737E',
    97: '0xb93bb7bd900e7567cdd868374288f05fee8b7ded',
  },
  masterChef: {
    56: '0xEb3047d30aEAB2A817394b468A9bcc98581B919c',
    97: '0x611FE1d0e4a14825A204976895c5C9C75381a906',
  },
  wbnb: {
    56: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
    97: '0xBaE36c04b80FE01bb1611160B199fACB7E3CdC27',
  },
  lottery: {
    56: '',
    97: '',
  },
  lotteryNFT: {
    56: '',
    97: '',
  },
  Wrap: {
    56: '0xc20dF561dDaDF52f0c8976550705250b3C47b607',
    97: '0x4Ee6C2e3Cf95acdDd7F309dc8b079c7ACF2AF848',
  },
  mulltiCall: {
    56: '0x1ee38d535d541c55c9dae27b12edf090c608e6fb',
    97: '0x67ADCB4dF3931b0C5Da724058ADC2174a9844412',
  },
  busd: {
    56: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    97: '0x8CE7720fD183AeC96b676FD8250724b05b0d7a6F',
  },
}
